package com.iwaki.springboot;

import lombok.Data;

@Data
public class ObjectForJsonGetUserName {
	String name;
	
	public ObjectForJsonGetUserName(String name) {
		this.name = name;
	}
	
	public ObjectForJsonGetUserName() {
	}
}
