package com.iwaki.springboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.iwaki.springboot.UserData;

@Repository
public interface UserDataRepository extends JpaRepository<UserData, Long> {

}
