package com.iwaki.springboot;

import javax.servlet.http.HttpServletRequest;

import lombok.Data;

@Data
public class RequestParams {
	private String id;
	private String exit_req;
	private String reload_req;
	private String join_req;
	private String userid;
	private String username;
	private String write_req;
	private String message;
	private String reload_message_req;
	private String reload_userlist_req;
	private String test_post;
	private String test_get;
	private String test_param;
	
	public RequestParams(HttpServletRequest request) {
		// test = request.getParameter("test");
		id = request.getParameter("id");
		exit_req = request.getParameter("exit_req");
		reload_req = request.getParameter("reload_req");
		join_req = request.getParameter("join_req");
		userid = request.getParameter("userid");
		username = request.getParameter("username");
		write_req = request.getParameter("write_req");	
		message = request.getParameter("message");
		reload_message_req = request.getParameter("reload_message_req");
		reload_userlist_req = request.getParameter("reload_userlist_req");
		test_post = request.getParameter("test_post");
		test_get = request.getParameter("test_get");
		test_param = request.getParameter("test_param");
	}
	
}
