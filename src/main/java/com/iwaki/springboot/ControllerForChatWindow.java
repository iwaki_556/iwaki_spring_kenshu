package com.iwaki.springboot;

import java.util.List;
import java.sql.Timestamp;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.iwaki.springboot.repositories.MsgDataRepository;
import com.iwaki.springboot.repositories.UserDataRepository;

@Transactional
@Controller
public class ControllerForChatWindow {
	@Autowired
	UserDataRepository repository;

	@Autowired
	MsgDataRepository repository_msg;

	@PersistenceContext
	EntityManager entityManager;

	UserDataDaoImpl dao;
	MsgDataDaoImpl dao_msg;

	Timestamp timestamp = new Timestamp(System.currentTimeMillis());

	@RequestMapping(value = "/chat", method = RequestMethod.POST)
	public ModelAndView chat(@Validated @ModelAttribute("formModel") UserData userdata, BindingResult result,
			@Validated @ModelAttribute("msgModel") MsgData msgdata, BindingResult result2, HttpServletResponse response,
			HttpServletRequest request, ModelAndView mav) {

		// @RequestParam(value = "exit_req", required = false) String exit_req 引数にかくのもアリ
		// リクエストパラメータを取得
		RequestParams rparams = new RequestParams(request);

		// joinリクエストの場合
		if (rparams.getJoin_req() != null) {
			if (rparams.getJoin_req().equals("true")) {

				// バリデータがエラーの時はエラー画面を返す
				if (result.hasErrors()) {
					mav.addObject("msg", "sorry, error is occured!!!");
					mav.setViewName("ChatTop");
					return mav;
				}

				// 未退室者で同一セッションID、同一名の場合は再入室処理
				Iterable<UserData> it2 = dao.findByNameWhereExitFalseAndSessionIdTrue(userdata.getName(),
						request.getRequestedSessionId());
				int countit2 = 0;
				for (UserData u : it2) {
					countit2++;
				}
				// 未退室者が１人以上の場合には、その内の一人として再入室
				for (UserData u : it2) {
					mav.addObject("name", u.getName());
					mav.addObject("userid", u.getUserid());
					mav.setViewName("ChatWindow");
					System.out.println("an user have re-joined to the chat: name = " + u.getName());
					// 入室者リスト取得
					Iterable<UserData> list = dao.findByExitFalse();
					mav.addObject("userlist", list);
					return mav;
				}

				// 同じ名前の人がいたらエラー
				if (dao.findByNameWithExitFalse(userdata.getName()).isEmpty() == false) {
					mav.addObject("msg", "error: the same username already exits, try with different name!!");
					mav.setViewName("ChatTop");
					Iterable<UserData> it3 = dao.findByNameWithExitFalse(userdata.getName());
					for (UserData x : it3) {
						System.out.println("join failure: the same of username exists = " + x.getName());
					}
					return mav;
				}

				// 問題なければ入室
				mav.setViewName("ChatWindow");
				// セッションID発行
				// Cookie cookie = new Cookie("visited", "2");
				// response.addCookie(cookie);

				// 未退室者で同一の名飴がある場合は、再入力を要求
				mav.addObject("msg", "chat window page.");
				userdata.setExit(false);// 退室フラグfalseにセット
				timestamp.setTime(System.currentTimeMillis());
				userdata.setJoin_time(timestamp); // 現在時刻をセット
				userdata.setExit_time(null); // 退室時刻はnull
				userdata.setSession(request.getRequestedSessionId());
				userdata.setUserid(HashGenerator.encryptStr(userdata.getName() + timestamp.toString()));
				repository.saveAndFlush(userdata);
				mav.addObject("name", userdata.getName());
				mav.addObject("userid", userdata.getUserid());

				// メッセージリスト取得
				Iterable<MsgData> list_msg = dao_msg.getAll();
				mav.addObject("msglist", list_msg);

				// 入室者リスト取得
				Iterable<UserData> list = dao.findByExitFalse();
				mav.addObject("userlist", list);
				System.out.println("an user has joined to the chat : name = " + userdata.getName());
				return mav;
			}
			// join_reqパラメータが変
			mav.setViewName("ChatError");
			mav.addObject("msg", "something happend.\n invalid request value for exit_req.");
			return mav;
		}

		// reloadリクエストの場合
		if (rparams.getReload_req() != null) {
			if (rparams.getReload_req().equals("true")) {

				mav.setViewName("ChatWindowIframePost");
				mav.addObject("msg", "reloaded!");
				mav.addObject("userid", userdata.getUserid());
				// セッションID発行
				// Cookie cookie = new Cookie("visited", "2");
				// response.addCookie(cookie);

				// DBとuseridに不整合がないかチェック
				if (dao.findByUserIdWithExitFalse(rparams.getUserid()).isEmpty()) {
					// DBに該当ID（ハッシュ）なし不正
					mav.setViewName("ChatError");
					mav.addObject("msg", "something happend.\n failed to reload.");
					return mav;
				} else {
					// 問題なし
					System.out.println("reloaded sucsessfully id = " + rparams.getUserid());
				}

				// メッセージリスト取得
				Iterable<MsgData> list_msg = dao_msg.getAll();
				mav.addObject("msglist", list_msg);

				// 入室者リスト取得
				Iterable<UserData> list = dao.findByExitFalse();
				mav.addObject("userlist", list);

				return mav;
			}
			// reload処理でエラー
			mav.setViewName("ChatError");
			mav.addObject("msg", "something happend.\n failed to reload.");
			return mav;
		}

		// writeリクエストの場合
		if (rparams.getWrite_req() != null) {
			if (rparams.getWrite_req().equals("true")) {

				mav.setViewName("ChatWindowIframePost");
				mav.addObject("msg", "wrote successfully!");
				mav.addObject("userid", userdata.getUserid());
				// セッションID発行
				// Cookie cookie = new Cookie("visited", "2");
				// response.addCookie(cookie);

				// DBとuseridに不整合がないかチェック
				if (dao.findByUserIdWithExitFalse(rparams.getUserid()).isEmpty()) {
					// DBに該当ID（ハッシュ）なし不正
					mav.setViewName("ChatError");
					mav.addObject("msg", "something happend.\n failed to write.");
					return mav;
				} else {
					// 問題なし
					System.out.println("wrote sucsessfully: id = " + rparams.getUserid());
				}

				timestamp.setTime(System.currentTimeMillis());
				msgdata.setTime(timestamp); // 現在時刻書き込み
				msgdata.setDeleted(false); // 消去フラグfalseにセット
				// id書き込み

				Iterable<UserData> it = dao.findByUserIdWithExitFalse(rparams.getUserid());
				for (UserData u : it) {
					msgdata.setUserdata(u);
					break;
				}
				repository_msg.saveAndFlush(msgdata);

				// メッセージリスト取得
				Iterable<MsgData> list_msg = dao_msg.getAll();
				mav.addObject("msglist", list_msg);

				// 入室者リスト取得
				Iterable<UserData> list = dao.findByExitFalse();
				mav.addObject("userlist", list);

				return mav;
			}
			// write処理でエラー
			mav.setViewName("ChatError");
			mav.addObject("msg", "something happend.\n failed to write.");
			return mav;
		}

		// リクエストの種類が不明な場合はエラー
		mav.setViewName("ChatError");
		mav.addObject("msg", "something happend.\n check for the request parameters.");
		return mav;

	}

	@RequestMapping(value = "/chat", method = RequestMethod.GET)
	public ModelAndView chat(ModelAndView mav) {

		mav.setViewName("ChatWindowIframeGet");
		mav.addObject("msg", "please wait...preparing chat window...");
		return mav;
	}

	@PostConstruct
	public void init() {
		dao = new UserDataDaoImpl(entityManager);
		dao_msg = new MsgDataDaoImpl(entityManager);

		UserData u1 = new UserData();
		MsgData m1 = new MsgData();

		if (dao.getAll().isEmpty() == true) {
			u1.setName("info");
			u1.setExit(true);
			u1.setExit_time(timestamp);
			u1.setSession("initialized");
			u1.setUserid("initialized");
			u1.setJoin_time(timestamp);
			repository.saveAndFlush(u1);
		}

		if (dao_msg.getAll().isEmpty() == true) {
			m1.setDeleted(true);
			m1.setMessage("initialized");
			m1.setTime(timestamp);
			m1.setUserdata(u1);
			repository_msg.saveAndFlush(m1);
		}

	}
}
