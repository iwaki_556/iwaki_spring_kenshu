package com.iwaki.springboot;



import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;


@Data
@Entity
@Table(name = "messages")
public class MsgData {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	@NotNull
	private long id;

	@Column(nullable = false)
	@NotEmpty
	private String message;

	@Column(nullable = false)
	private boolean deleted;

	@Column(nullable = false)
	private Timestamp time;

	@ManyToOne
	private UserData userdata;



}
