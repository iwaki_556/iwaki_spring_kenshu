package com.iwaki.springboot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class ListConverter {
	/**
	 * @see Collection#toArray(Object[])
	 */
	public static <T> T[] convert(Collection<T> c, T[] array){
		return c.toArray(array);
	}

	/**
	 * @see ArrayList(Collection)
	 * @see Arrays#asList(Object...)
	 */
	public static <T> List<T> convert(T[] array){
		return new ArrayList<T>(Arrays.asList(array));
	}
	
	/**
	 * @see Collection
	 */
	public static <T> List<T> copy(Collection<T> c){
		return new ArrayList<T>(c);
	}
	
	/**
	 * @see Arrays#copyOf(Object[], int)
	 */
	public static <T> T[] copy(T[] array){
		return Arrays.copyOf(array, array.length);
	}
}