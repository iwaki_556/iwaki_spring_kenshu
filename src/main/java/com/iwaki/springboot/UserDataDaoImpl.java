package com.iwaki.springboot;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;

@Repository
public class UserDataDaoImpl implements UserDataDao<UserData> {
	// private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private EntityManager entityManager;

	public UserDataDaoImpl() {
		super();
	}

	public UserDataDaoImpl(EntityManager manager) {
		entityManager = manager;
	}

	// 全要素取得
	@Override
	public List<UserData> getAll() {
		Query query = entityManager.createQuery("from UserData");
		List<UserData> list = query.getResultList();
		entityManager.close();
		return list;
	}

	// IDで検索
	@Override
	public UserData findById(long id) {
		return (UserData) entityManager.createQuery("from UserData where id = " + id).getSingleResult();
	}

	// sessionIDで検索(複数)(EXITフラグがFalseの場合）
	@Override
	public List<UserData> findBySessionIdWithExitFalse(String sessionid) {
		return entityManager.createQuery("from UserData where session like :param1 and exit = false")
				.setParameter("param1", sessionid).getResultList();
	}

	// sessionIDで検索(複数)(EXITフラグがFalseの場合）
	@Override
	public int setExitFlagBySessionId(String sessionid) {
		// return entityManager.createQuery("update UserData u set u.exit = true where
		// u.session like :param1 and u.exit = false").setParameter("param1",
		// sessionid).executeUpdate();

		// CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		// CriteriaUpdate<UserData> update =
		// builder.createCriteriaUpdate(UserData.class);
		// Root<UserData> root = update.from(UserData.class);
		// update.set(root.get("exit"), true).where(builder.equal(root.get("session"),
		// sessionid));
		// return entityManager.createQuery(update).executeUpdate();

		return entityManager
				.createQuery("update UserData u set u.exit = true where u.session like :param1 and u.exit = false")
				.setParameter("param1", sessionid).executeUpdate();
		// return entityManager.createQuery("delete from UserData u where u.id =
		// 10").executeUpdate();

	}

	// 該当セッションIDの退室時刻を入力
	public int setExitTimeBySessionId(Timestamp exit_time, String sessionid) {
		return entityManager.createQuery("update UserData u set u.exit_time = :param1 where u.session like :param2")
				.setParameter("param1", exit_time).setParameter("param2", sessionid).executeUpdate();
	}

	// 該当ユーザーハッシュIDの退室時刻を入力
	public int setExitTimeByUserId(Timestamp exit_time, String userid) {
		return entityManager.createQuery("update UserData u set u.exit_time = :param1 where u.userid like :param2")
				.setParameter("param1", exit_time).setParameter("param2", userid).executeUpdate();
	}

	// 該当ユーザーハッシュIDの退室フラグを立てる
	public int setExitFlagByUserId(String userid) {
		return entityManager
				.createQuery("update UserData u set u.exit = true where u.userid like :param1 and u.exit = false")
				.setParameter("param1", userid).executeUpdate();
	}

	// 同じ名前のユーザーがいるか確認（退室者除く）
	public List<UserData> findByNameWithExitFalse(String name) {
		return entityManager.createQuery("from UserData where name like :param1 and exit = false")
				.setParameter("param1", name).getResultList();

	}
	
	// userIDで検索(複数)(EXITフラグがFalseの場合）
	@Override
	public List<UserData> findByUserIdWithExitFalse(String userid) {
		return entityManager.createQuery("from UserData where userid like :param1 and exit = false")
				.setParameter("param1", userid).getResultList();
	}
	
	// 未退室者をすべて抽出
	@Override
	public List<UserData> findByExitFalse() {
		return entityManager.createQuery("from UserData where exit = false").getResultList();
	}
	
	// 同じ名前のユーザーがいるか確認（退室者除く）(セッションIDが同じなら
	public List<UserData> findByNameWhereExitFalseAndSessionIdTrue(String name, String sessionid) {
		return entityManager.createQuery("from UserData where name like :param1 and exit = false and session like :param2")
				.setParameter("param1", name).setParameter("param2", sessionid).getResultList();

	}

}
