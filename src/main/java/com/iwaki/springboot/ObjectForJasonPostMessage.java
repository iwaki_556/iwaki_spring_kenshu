package com.iwaki.springboot;

import java.io.Serializable;

import lombok.Data;

@Data
public class ObjectForJasonPostMessage implements Serializable {

	private static final long serialVersionUID = 1L;
		
	private String userid;
	private String message;


}