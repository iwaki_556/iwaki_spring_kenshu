package com.iwaki.springboot;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class ObjectForJsonGetMessages {
	long id;
	String name;
	String message;
	Timestamp time;
	
	public ObjectForJsonGetMessages(long id, String name, String message, Timestamp time) {
		this.id = id;
		this.name = name;
		this.message = message;
		this.time = (Timestamp)time.clone();
	}
	
	public ObjectForJsonGetMessages() {
	}
}
