package com.iwaki.springboot;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

@Repository
public class MsgDataDaoImpl implements MsgDataDao<MsgData> {

	private static final long serialVersionUID = 1L;
	private EntityManager entityManager;

	public MsgDataDaoImpl() {
		super();
	}

	public MsgDataDaoImpl(EntityManager manager) {
		entityManager = manager;
	}
	
	
	@Override
	public List<MsgData> getAll() {
		Query query = entityManager.createQuery("from MsgData order by time asc");
		List<MsgData> list = query.getResultList();
		entityManager.close();
		return list;
	}
	
	@Override
	public List<MsgData> findWhereDeletedFalse() {
		Query query = entityManager.createQuery("from MsgData where deleted = false order by time asc");
		List<MsgData> list = query.getResultList();
		entityManager.close();
		return list;
	}
	
	@Override
	public MsgData findById(Long id){
		return (MsgData) entityManager.createQuery("from MsgData where id = " + id).getSingleResult();
	}
	
	
	
}
