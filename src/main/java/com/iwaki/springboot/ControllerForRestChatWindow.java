package com.iwaki.springboot;

import java.sql.Timestamp;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.iwaki.springboot.repositories.UserDataRepository;

@Transactional
@Controller
public class ControllerForRestChatWindow {
	@Autowired
	UserDataRepository repository;

	@PersistenceContext
	EntityManager entityManager;

	UserDataDaoImpl dao;

	Timestamp timestamp = new Timestamp(System.currentTimeMillis());

	@RequestMapping(value = "/rchat", method = RequestMethod.POST)
	public ModelAndView rchat(
			@Validated @ModelAttribute("formModel") UserData userdata, BindingResult result,
			HttpServletResponse response, HttpServletRequest request, ModelAndView mav) {

		//リクエストパラメータを取得
		RequestParams rparams = new RequestParams(request);
		
		// バリデータがエラーの時はエラー画面を返す
		if (result.hasErrors()) {
			mav.addObject("msg", "sorry, error is occured!!!");
			mav.setViewName("ChatTop");
			return mav;
		}

		// 同じ名前の人がいたらエラー
		if (dao.findByNameWithExitFalse(userdata.getName()).isEmpty() == false) {
			mav.addObject("msg", "error: the same username already exits, try with different name!!");
			mav.setViewName("ChatTop");
			Iterable<UserData> it = dao.findByNameWithExitFalse(userdata.getName());
			for (UserData x : it) {
				System.out.println(x.getName());
			}
			return mav;
		}

		// 問題なければ入室
		mav.setViewName("RestChatWindow");
		// セッションID発行
		// Cookie cookie = new Cookie("visited", "2");
		// response.addCookie(cookie);

		// 未退室者で同一の名飴がある場合は、再入力を要求
		mav.addObject("msg", "joined!");
		userdata.setExit(false);// 退室フラグfalseにセット
		timestamp.setTime(System.currentTimeMillis());
		userdata.setJoin_time(timestamp); // 現在時刻をセット
		userdata.setExit_time(null); // 退室時刻はnull
		userdata.setSession(request.getRequestedSessionId());
		userdata.setUserid(HashGenerator.encryptStr(userdata.getName() + timestamp.toString()));
		repository.saveAndFlush(userdata);
		mav.addObject("name", userdata.getName());
		mav.addObject("userid", userdata.getUserid());

		return mav;

	}

	@PostConstruct
	public void init() {
		dao = new UserDataDaoImpl(entityManager);
	}
}
