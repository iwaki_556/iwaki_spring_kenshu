package com.iwaki.springboot;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public interface UserDataDao<T> extends Serializable {
	public List<T> getAll();

	public UserData findById(long id);

	public List<T> findBySessionIdWithExitFalse(String sessionid);

	public int setExitFlagBySessionId(String sessionid);

	public int setExitTimeBySessionId(Timestamp exit_time, String sessionid);

	public int setExitTimeByUserId(Timestamp exit_time, String userid);

	public int setExitFlagByUserId(String userid);

	public List<T> findByNameWithExitFalse(String name);
	
	public List<T> findByUserIdWithExitFalse(String userid);

	public List<T> findByExitFalse();

	public List<T> findByNameWhereExitFalseAndSessionIdTrue(String name, String sessionid);
	
	
}
