package com.iwaki.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
public class MybootAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MybootAppApplication.class, args);
	}
}
