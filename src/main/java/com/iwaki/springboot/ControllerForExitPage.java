package com.iwaki.springboot;

import java.sql.Timestamp;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iwaki.springboot.repositories.UserDataRepository;

@Transactional
@Controller
public class ControllerForExitPage {
	@Autowired
	UserDataRepository repository;

	@PersistenceContext
	EntityManager entityManager;

	UserDataDaoImpl dao;

	Timestamp timestamp = new Timestamp(System.currentTimeMillis());

	// Exit画面
	@RequestMapping(value = "/exit", method = RequestMethod.POST)
	public ModelAndView exit( HttpServletResponse response,
			HttpServletRequest request, ModelAndView mav) {
		
		//リクエストパラメータを取得
		RequestParams rparams = new RequestParams(request);
		
		if (rparams.getExit_req() != null) {
			if (rparams.getExit_req().equals("true")) {

				mav.setViewName("ChatExit");
				mav.addObject("msg", "see you!");

				// Iterable<UserData> alluserdata =
				// dao.findBySessionIdWithExitFalse(request.getRequestedSessionId()); 
				// for(UserData x : alluserdata) {
				// System.out.println(x.getName());
				// }

				// 退室者のセッションIDに該当する退室フラグをTRUEにする
				// dao.setExitFlagBySessionId(request.getRequestedSessionId());
				// 退室者のUseriD（ハッシュ）に該当する退室フラグをTRUEにする
				dao.setExitFlagByUserId(rparams.getUserid());
				// 退室時刻を更新
				// dao.setExitTimeBySessionId(exit_time, sessionid)
				timestamp.setTime(System.currentTimeMillis());
				// dao.setExitTimeBySessionId(timestamp, request.getRequestedSessionId());
				dao.setExitTimeByUserId(timestamp, rparams.getUserid());
				
				System.out.println("exited sucsessfully id = " + rparams.getUserid());
				return mav;

			}
			// exit_reqのパラメータが変
			mav.setViewName("ChatError");
			mav.addObject("msg", "something happend.\n invalid value of exit_req.");
			return mav;
			
		}
		mav.addObject("msg", "bad request!");
		mav.setViewName("ChatExit");
		return mav;
	}

	@PostConstruct
	public void init() {
		dao = new UserDataDaoImpl(entityManager);
	}
}
