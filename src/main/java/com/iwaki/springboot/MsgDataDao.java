package com.iwaki.springboot;

import java.io.Serializable;
import java.util.List;

public interface MsgDataDao<T> extends Serializable {
	public List<T> getAll();
	public List<MsgData> findWhereDeletedFalse();
	public MsgData findById(Long id);

}
