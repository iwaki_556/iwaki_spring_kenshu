package com.iwaki.springboot;

import lombok.Data;

@Data
public class ObjectForJasonDeleteMessage {

	private String delete;
	private String userid;
	private String msgid;
}
