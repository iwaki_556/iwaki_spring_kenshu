package com.iwaki.springboot;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;
@Data
@Entity
@Table(name = "users")
public class UserData {
	

	@OneToMany(cascade = CascadeType.ALL)
	@Column(nullable = true)
	private List<MsgData> msgdatas;

	public List<MsgData> getMsgdatas() {
		return msgdatas;
	}

	public void setMsgdatas(List<MsgData> msgdatas) {
		this.msgdatas = msgdatas;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@NotNull
	private long id;

	@Column(nullable = false)
	@Length(min = 1, max = 8)
	@NotEmpty(message = "user name must not be null!")
	private String name;

	@Column(nullable = false)
	private String session;

	@Column(nullable = false)
	private Timestamp join_time;

	@Column(nullable = true)
	private Timestamp exit_time;

	@Column(nullable = false)
	private boolean exit;

	@Column(nullable = false)
	private String userid;

}