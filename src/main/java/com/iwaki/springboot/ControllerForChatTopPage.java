package com.iwaki.springboot;

import java.sql.Timestamp;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iwaki.springboot.repositories.UserDataRepository;

@Transactional
@Controller
public class ControllerForChatTopPage {
	@Autowired
	UserDataRepository repository;

	@PersistenceContext
	EntityManager entityManager;

	UserDataDaoImpl dao;

	Timestamp timestamp = new Timestamp(System.currentTimeMillis());

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView topPage(@ModelAttribute("formModel") UserData userdata, HttpServletRequest request,
			ModelAndView mav) {
		
		//リクエストパラメータを取得
		RequestParams rparams = new RequestParams(request);
		
		mav.setViewName("ChatTop");
		mav.addObject("msg", "enter your name, then join!");
		request.getSession(true); // セッションID発行
		return mav;

	}

	// @PostConsructでコンストラクタ実行後に自動的に呼ばれるメソッドを作り、DAOオブジェクトにEntityManagerを渡します。
	@PostConstruct
	public void init() {
		dao = new UserDataDaoImpl(entityManager);
	}

}
