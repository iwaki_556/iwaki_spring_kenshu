package com.iwaki.springboot;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.iwaki.springboot.repositories.MsgDataRepository;
import com.iwaki.springboot.repositories.UserDataRepository;

@RestController
public class ControllerForRestChatTest {

	@Autowired
	UserDataRepository repository_usr;

	@Autowired
	MsgDataRepository repository_msg;

	@PersistenceContext
	EntityManager entityManager;

	UserDataDaoImpl dao_usr;
	MsgDataDaoImpl dao_msg;

	Timestamp timestamp = new Timestamp(System.currentTimeMillis());

	// FindByUserIdの結果をJSON形式でリターン
	@RequestMapping(value = "/rchat/test/return_single_name", method = RequestMethod.GET)
	public ObjectForJsonGetUserName RestChatGetName(HttpServletResponse response, HttpServletRequest request) {

		// リクエストパラメータを取得
		RequestParams rparams = new RequestParams(request);

		String nameFromUserData = "error";
		Iterable<UserData> users = dao_usr.findByUserIdWithExitFalse(rparams.getUserid());
		for (UserData user : users) {
			nameFromUserData = user.getName();
			break;
		}
		ObjectForJsonGetUserName name = new ObjectForJsonGetUserName(nameFromUserData);

		return name;

	}

	// ユーザーリストをJSON形式でリターン
	@RequestMapping(value = "/rchat/test/return_userlist", method = RequestMethod.GET)
	public List<ObjectForJsonGetUserName> RestChatGetUsers(HttpServletResponse response, HttpServletRequest request) {

		List<ObjectForJsonGetUserName> usernames = new ArrayList<ObjectForJsonGetUserName>();
		Iterable<UserData> users = dao_usr.findByExitFalse();
		for (UserData user : users) {
			usernames.add(new ObjectForJsonGetUserName(user.getName()));
		}

		return usernames;
	}

	// メッセージをJSON形式でリターン
	@RequestMapping(value = "/rchat/test/return_messages", method = RequestMethod.GET)
	public List<ObjectForJsonGetMessages> RestChatGetMessages(HttpServletResponse response,
			HttpServletRequest request) {

		List<ObjectForJsonGetMessages> messages = new ArrayList<ObjectForJsonGetMessages>();
		Iterable<MsgData> messages_iterable = dao_msg.findWhereDeletedFalse();
		for (MsgData message : messages_iterable) {
			messages.add(new ObjectForJsonGetMessages(message.getId(), message.getUserdata().getName(),
					message.getMessage(), message.getTime()));
		}

		return messages;
	}

	// メッセージ追加
	@RequestMapping(value = "/rchat/test/post_message", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public boolean RestChatPostMessage(@RequestBody ObjectForJasonPostMessage bean, HttpServletResponse response,
			HttpServletRequest request) {

		System.out.println("メッセージ= " + bean.getMessage());
		System.out.println("ユーザID= " + bean.getUserid());

		// 識別IDが存在するかリストを取得(複数取得されたときにエラーが出ると困るので、リストで取得）
		List<UserData> users = dao_usr.findByUserIdWithExitFalse(bean.getUserid());
		if (users.isEmpty()) {
			return false;
		} else {
			for (UserData user : users) {
				MsgData message = new MsgData();
				message.setDeleted(false);
				message.setMessage(bean.getMessage());
				timestamp.setTime(System.currentTimeMillis());
				message.setTime(timestamp);
				message.setUserdata(user);
				repository_msg.saveAndFlush(message);
				return true;
			}
		}

		return false;

		// timestamp.setTime(System.currentTimeMillis());
		// msgdata.setTime(timestamp); // 現在時刻書き込み
		// msgdata.setDeleted(false); // 消去フラグfalseにセット

		/*
		 * Javascript側の記載
		 * 
		 * 
		 * <script> // 各フィールドから値を取得してJSONデータを作成 var data = { "name": "hoge", };
		 * 
		 * // 通信実行 $.ajax({ type:"post",
		 * url:"http://localhost:8080/rchat/test/post_message",
		 * data:JSON.stringify(data), contentType: 'application/json', dataType: "json",
		 * success: function(json_data) { // 成功時の処理 } });
		 * 
		 * </script>
		 */

		/*
		 * クラスの書き方
		 * 
		 * package com.iwaki.springboot; import java.io.Serializable; public class
		 * SampleBean implements Serializable { private static final long
		 * serialVersionUID = 1L; private String name; public String getName() { return
		 * name; } public void setName(String name) { this.name = name; } }
		 */
	}

	// メッセージ削除
	@RequestMapping(value = "/rchat/test/delete_message", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public boolean RestChatDeleteMessage(@RequestBody ObjectForJasonDeleteMessage bean, HttpServletResponse response,
			HttpServletRequest request) {

		System.out.println("delete= " + bean.getDelete());
		System.out.println("ユーザID= " + bean.getUserid());
		System.out.println("msgid= " + bean.getMsgid());

		// 識別IDが存在するかリストを取得(複数取得されたときにエラーが出ると困るので、リストで取得）
		List<UserData> users = dao_usr.findByUserIdWithExitFalse(bean.getUserid());
		if (users.isEmpty()) {
			System.out.println("kuso");
			
			return false;
		} else {
			dao_msg.findById(Long.parseLong(bean.getMsgid())).setDeleted(true);
			System.out.println(dao_msg.findById(Long.parseLong(bean.getMsgid())).getMessage());
			repository_msg.flush();
			System.out.println("ok");
			return true;
		}

		// timestamp.setTime(System.currentTimeMillis());
		// msgdata.setTime(timestamp); // 現在時刻書き込み
		// msgdata.setDeleted(false); // 消去フラグfalseにセット

		/*
		 * Javascript側の記載
		 * 
		 * 
		 * <script> // 各フィールドから値を取得してJSONデータを作成 var data = { "name": "hoge", };
		 * 
		 * // 通信実行 $.ajax({ type:"post",
		 * url:"http://localhost:8080/rchat/test/post_message",
		 * data:JSON.stringify(data), contentType: 'application/json', dataType: "json",
		 * success: function(json_data) { // 成功時の処理 } });
		 * 
		 * </script>
		 */

		/*
		 * クラスの書き方
		 * 
		 * package com.iwaki.springboot; import java.io.Serializable; public class
		 * SampleBean implements Serializable { private static final long
		 * serialVersionUID = 1L; private String name; public String getName() { return
		 * name; } public void setName(String name) { this.name = name; } }
		 */
	}

	@PostConstruct
	public void init() {
		dao_usr = new UserDataDaoImpl(entityManager);
		dao_msg = new MsgDataDaoImpl(entityManager);

	}
}
